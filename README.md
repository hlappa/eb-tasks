# EB Tasks

This repository holds implementation for EB Task 1 and bonus

## Prerequisites

- Docker
- Docker-compose

## Get started

```bash
$ docker-compose build
$ docker-compose up
```

After the container is up and running, you can access the endpoints `http://localhost:3000/api`
