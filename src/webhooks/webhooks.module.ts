import { Module } from "@nestjs/common"
import { Deals } from "../db/firebase/deals"
import { HubspotDeals } from "../lib/hubspot/deals"
import { DealsProcessor } from "./processors/deals"
import { WebhooksController } from "./webhooks.controller"
import { WebhooksService } from "./webhooks.service"

@Module({
  controllers: [WebhooksController],
  providers: [WebhooksService, DealsProcessor, HubspotDeals, Deals]
})
export class WebhooksModule {}
