export class WebhookEvent {
  objectId: number
  propertyName: string
  propertyValue: string
  changeSource: string
  eventId: number
  subscriptionId: number
  portalId: number
  appId: number
  occurredAt: number
  eventType: string
  attemptNumber: number
}
