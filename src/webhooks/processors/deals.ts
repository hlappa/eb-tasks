import { Injectable } from "@nestjs/common"
import { Deals } from "src/db/firebase/deals"
import { HubspotDeals } from "../../lib/hubspot/deals"
import { WebhookEvent } from "../dto/event.dto"

@Injectable()
export class DealsProcessor {
  constructor(
    private readonly hsDeals: HubspotDeals,
    private readonly deals: Deals
  ) {}

  async handleDealEvent(event: WebhookEvent) {
    switch (event.eventType.split(".")[1]) {
      case "creation":
        await this.createDealToDb(event.objectId.toString())
        break
      case "propertyChange":
        await this.updateDealToDb(event)
        break
    }
  }

  private async createDealToDb(id: string) {
    const deal = await this.hsDeals.getById(id)

    if (!deal) {
      console.error(`Could not find deal from Hubspot with id ${id}`)
      return
    }

    await this.deals.create(deal.properties)
  }

  private async updateDealToDb(event: WebhookEvent) {
    await this.deals.update(event.objectId.toString(), {
      [event.propertyName]: event.propertyValue
    })
  }
}
