import { Body, Controller, Post } from "@nestjs/common"
import { WebhookEvent } from "./dto/event.dto"
import { WebhooksService } from "./webhooks.service"

@Controller("webhooks")
export class WebhooksController {
  constructor(private readonly whService: WebhooksService) {}

  @Post()
  async incomingBatch(@Body() body: Array<WebhookEvent>) {
    for (const event of body) await this.whService.handleEvent(event)
    return
  }
}
