import { Injectable } from "@nestjs/common"
import { WebhookEvent } from "./dto/event.dto"
import { DealsProcessor } from "./processors/deals"

@Injectable()
export class WebhooksService {
  constructor(private readonly dealProcessor: DealsProcessor) {}
  async handleEvent(event: WebhookEvent) {
    switch (event.eventType.split(".")[0]) {
      case "deal":
        await this.dealProcessor.handleDealEvent(event)
        break

      // Other possible objects...
    }
  }
}
