import { Module } from "@nestjs/common"
import { HealthModule } from "./health/health.module"
import { MeetingSummaryModule } from "./meeting/summary/summary.module"
import { WebhooksModule } from "./webhooks/webhooks.module"

@Module({
  imports: [HealthModule, WebhooksModule, MeetingSummaryModule],
  controllers: [],
  providers: []
})
export class AppModule {}
