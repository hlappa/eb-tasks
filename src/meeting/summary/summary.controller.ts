import { BadRequestException, Controller, Param, Post } from "@nestjs/common"
import { Summary, TSummary } from "../../db/firebase/summaries"
import { HubspotMeetings } from "../../lib/hubspot/meetings"
import { GetMeeting } from "../../lib/hubspot/types/meeting"

@Controller("meeting")
export class MeetingSummaryController {
  constructor(
    private readonly summary: Summary,
    private readonly hsMeetings: HubspotMeetings
  ) {}

  @Post("/:id/summary/sync")
  async sync(@Param("id") id: string) {
    const summaryData: TSummary = await this.summary.getByMeetingId(id)

    if (!summaryData)
      throw new BadRequestException("Didn't find any summaries with given ID")

    const meeting: GetMeeting = await this.hsMeetings.getById(id)

    if (!meeting)
      throw new BadRequestException("Didn't find meeting with given ID")

    const res = await this.hsMeetings.update(meeting.id, {
      properties: {
        hs_internal_meeting_notes: summaryData.text
      }
    })

    if (!res)
      throw new BadRequestException(
        "Could not update internal notes for meeting"
      )

    return
  }
}
