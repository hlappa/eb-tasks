import { Module } from "@nestjs/common"
import { HubspotMeetings } from "../../lib/hubspot/meetings"
import { Summary } from "../../db/firebase/summaries"
import { MeetingSummaryController } from "./summary.controller"

@Module({
  controllers: [MeetingSummaryController],
  providers: [Summary, HubspotMeetings]
})
export class MeetingSummaryModule {}
