import { Injectable } from "@nestjs/common"
import hsClient from "../client"
import { GetDeal } from "../types/deal"

@Injectable()
export class HubspotDeals {
  async getById(id: string): Promise<GetDeal | undefined> {
    const res = await hsClient.get(`/deals/${id}`, {
      params: {
        archived: false,
        properties:
          // eslint-disable-next-line max-len
          "amount,closedate,createdate,dealname,dealstage,hs_lastmodifieddate,hs_object_id,pipeline"
      }
    })

    if (!res.data) return undefined

    return res.data
  }
}
