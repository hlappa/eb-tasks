export type HubspotBase = {
  id: string
  createdAt: Date
  updatedAt: Date
  archived: boolean
}
