import { HubspotBase } from "./base"

export type MeetingProperties = {
  hs_timestamp: Date
  hs_meeting_title: string
  hubspot_owner_id: string
  hs_meeting_body: string
  hs_internal_meeting_notes: string
  hs_meeting_external_url: string
  hs_meeting_location: string
  hs_meeting_start_time: Date
  hs_meeting_end_time: Date
  hs_meeting_outcome: string
}

export type GetMeeting = {
  properties: {
    hs_createdate: Date
    hs_lastmodifieddate: Date
    hs_object_id: string
  } & MeetingProperties
} & HubspotBase
