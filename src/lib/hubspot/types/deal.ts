import { HubspotBase } from "./base"

export type Deal = {
  amount: string
  closedate: Date
  createdate: Date
  dealname: string
  dealstage: string
  hs_lastmodifieddate: Date
  hs_object_id: string
  pipeline: string
}

export type GetDeal = {
  properties: Deal
} & HubspotBase
