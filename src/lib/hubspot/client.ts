import axios, { AxiosInstance } from "axios"

const hsClient: AxiosInstance = axios.create({
  baseURL: process.env.HUBSPOT_URL,
  timeout: 2000,
  headers: {
    Authorization: `Bearer ${process.env.HUBSPOT_TOKEN}`
  }
})

hsClient.interceptors.request.use(function (config) {
  return config
})

hsClient.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    if (error.response) {
      console.error(error.response.data)
    } else {
      console.error(error)
    }

    return Promise.reject(error)
  }
)

export default hsClient
