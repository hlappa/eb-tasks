import hsClient from "../client"
import { GetMeeting, MeetingProperties } from "../types/meeting"

export class HubspotMeetings {
  async getById(id: string): Promise<GetMeeting | undefined> {
    const res = await hsClient.get(`/meetings/${id}`, {
      params: {
        archived: false,
        properties:
          // eslint-disable-next-line max-len
          "hs_timestamp,hs_meeting_title,hubspot_owner_id,hs_meeting_body,hs_internal_meeting_notes,hs_meeting_external_url,hs_meeting_location,hs_meeting_start_time,hs_meeting_end_time,hs_meeting_outcome"
      }
    })

    if (!res.data) return undefined

    return res.data
  }

  async update(
    id: string,
    payload: { properties: Partial<MeetingProperties> }
  ) {
    const res = await hsClient.patch(`/meetings/${id}`, payload)

    return res.status === 200
  }
}
