import { initializeApp } from "firebase-admin/app"
import { getFirestore } from "firebase-admin/firestore"

const serviceAccount = JSON.parse(process.env.SERVICE_ACCOUNT)

// eslint-disable-next-line @typescript-eslint/no-var-requires
const admin = require("firebase-admin")

const config = {
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.DATABASE_URL
}

const firebase = initializeApp(config)
const db = getFirestore(firebase)
export default db
