import { Injectable } from "@nestjs/common"
import db from "./client"

export type TSummary = {
  text: string
  meeting_id: string
}

@Injectable()
export class Summary {
  async getByMeetingId(id: string): Promise<TSummary> | undefined {
    const snapshot = await db
      .collection("summaries")
      .limit(1)
      .where("meeting_id", "==", id)
      .get()

    return snapshot.docs[0]?.data() as TSummary
  }
}
