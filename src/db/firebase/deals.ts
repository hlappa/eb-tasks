import { Injectable } from "@nestjs/common"
import { Deal } from "src/lib/hubspot/types/deal"
import db from "./client"

@Injectable()
export class Deals {
  async getById(id: string) {
    const snapshot = await db
      .collection("deals")
      .limit(1)
      .where("hs_object_id", "==", id)
      .get()

    return snapshot.docs[0]?.data() as Deal
  }

  async create(data: Deal) {
    const docRef = db.collection("deals").doc(data.hs_object_id)
    const res = await docRef.set(data)

    if (!res.writeTime) console.error("Could not write document to Firestore!")
  }

  async update(id: string, data: Partial<Deal>) {
    const docRef = db.collection("deals").doc(id)
    const res = await docRef.update(data)

    if (!res.writeTime) console.error("Could not update document to Firestore!")
  }
}
