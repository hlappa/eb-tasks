FROM node:18.14.0-alpine3.16

RUN npm i -g npm@9.x.x

WORKDIR /opt/app

COPY src ./src
COPY test ./test
COPY package.json .
COPY package-lock.json .
COPY tsconfig.json .
COPY tsconfig.build.json .

RUN npm i \
  && chown -R node:node /opt/app

USER node

CMD npm run start:dev
